![logo Cardio Sync](https://trello-attachments.s3.amazonaws.com/5deb0aa40f086e4b5b2bf8aa/360x640/7ad09a2ab685719a88fa7818f2924d18/Tela_Inicial.jpg)

# About App Creation

The CardioSync app came up inside Shawee's Hackathon Total Voice. The proposed challenge seeks to implement a solution with the inclusion of the Total Voice API.

# Description

Cardio Sync enables the pacemaker patient to choose an emergency contact to receive a call if a potential risk situation such as bradycardia or tachycardia is identified, making it feasible for him to receive the necessary assistance in a timely manner.

In addition, the solution provides information to the physician about the patient's medications, symptoms presented by the patient, and data on pacemaker functioning, for example.

The app gathers features so that the patient can keep the real-time analysis of their clinical condition, enabling them to perform preventive actions, allowing risk situations to be minimized through this observation.

# Justification

In 2016, according to the G1, more than 300,000 people used pacemakers in Brazil, and every year around 49,000 new devices were deployed. For every million inhabitants in Brazil, there are on average 199 mobile devices. This number is still considered very low compared to other Latin American countries, such as Chile and Uruguay, which have twice as many pacemakers per million inhabitants, and developed countries as the USA, which has eight times the amount of Brazil. Nevertheless, the market in Brazil is well established and large in the Southeast. Other than that, the largest number of implants occurs in people between 50 and 79 years old, with their peak around 60 years.

It is common for pacemaker users to find it difficult to get constant medical follow-up on their medical condition, which can limit their quality of life, as many patients complain of their fear of physical activity, or even, to travel and suffer some kind of malaise.

The application then aims to bring more safety and reliability to patients, that their state of health is under control, which restores their self-confidence and, consequently, restoring their quality of life and motivation.

# Technical Detailing

*Ionic 4

# Versions

[Android](https://gitlab.com/hackgrupo71/hackapp)

# Team

|![Lincoln](https://media.licdn.com/dms/image/C4D03AQFuxRhy5eDqXA/profile-displayphoto-shrink_800_800/0?e=1581552000&v=beta&t=mrTNXtrMU7-IgQKFU-IrfgMq9BuBbzsSZC-dpkFmlFo) | ![Júlia](https://trello-avatars.s3.amazonaws.com/84201306b0a91268d13593854d4e9089/original.png) | ![Beatriz](https://trello-avatars.s3.amazonaws.com/3d7af43f6deb339fa5de8eb97a85af9e/original.png) | ![Wagner](https://trello-avatars.s3.amazonaws.com/e0c842f5edd6a226f802bc1c085568af/original.png) | ![Antonino](https://avatars3.githubusercontent.com/u/4805292?s=400&v=4) |
|:---------------------:|:------------------:|:----------------:|:--------------------:|:-----------------------:|
[Lincoln](https://github.com/Lincolntec)  | [Júlia]()| [Beatriz]() | [Wagner](https://github.com/wagnerstefani) | [Antonino](https://github.com/tonino00)| 
| Dev | Design| Business | Dev | Dev |